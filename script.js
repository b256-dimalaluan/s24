console.log("hello");

let getCube = Math.pow(2, 3);

let word1 = 'The';
let word2 = 'cube';
let word3 = 'of';
let word4 = 'two';
let word5 = 'is';
let sentence =`${word1} ${word2} ${word3} ${word4} ${word5} ${getCube}`;
console.log(sentence);



// Create a variable address with a value of an array containing details of an address.
// Destructure the array and print out a message with the full address using Template Literals.

let fullAddress = ['Blk 19, Lot 10', 'Camella Homes', 'Sambat', 'San Pascual', 'Batangas', 'Philippines'];

const [houseNum, subd, brgy, municipality, province, country] = fullAddress;


console.log(`I live at ${houseNum}, ${subd}, ${brgy}, ${municipality}, ${province}, ${country}`)


/*let houseNum = fullAddress[0];
let subd = fullAddress[1];
let brgy = fullAddress[2];
let municipality = fullAddress[3];
let province = fullAddress[4];
let country = fullAddress[5];

let sentenceAddress = `I live in ${houseNum}, ${subd}, ${brgy}, ${municipality}, ${province}, ${country}`;

console.log(sentenceAddress);*/

function dog (name, age, breed) {
	this.name = name;
	this.age = age;
	this.breed = breed;
};

let dog1 = new dog('Jap', 13, 'Lab-Pitbull mixed');

let dog2 = new dog('Juan', 3, 'Dalmatian')

let dog3 = new dog('Tagpe', 7, 'Aspin');

console.log(dog3);

console.log("My first dog as a kid was " + dog1.name + ", and he was with our family for " + dog1.age + " years. He was a " + dog1.breed);
console.log(`My first dog as an adult was ${dog2.name} he's a ${dog2.breed} and I adopted him as my friend cannot attend to extra 5 puppies that her dog birthed. Sadly, he only managed to lived up to ${dog2.age} years`);

console.log(`My current dog that is currently my guard dog that is in our garage watching over my bicycles and motorcycle is ${dog3.name}. He looks like a Jack Russel Terrier, but he's really a ${dog3.breed}. ${dog3.name} is currently ${dog3.age} years old but still acts like a puppy and I loved it.`)


// Create an array of numbers.
// Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.

let numbers = [1, 2, 3, 4, 5];
// forEach
numbers.forEach(function(number){
	console.log(number);
});

//Arrow function
let numbersArrow = numbers.reduce((x, y) => {

	return x + y;
});
console.log(numbersArrow);

//Implicit return
let numbersImplicit = numbers.reduce ((x, y) => x + y);

console.log(numbersImplicit);

